/* //import * as config from './config.json';
import * as db from '../db/index';
import * as interfaces from '../interfaces/index';
import * as ccxt from 'ccxt';
import * as ccxws from 'ccxws';

const debug = false

let sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

async function saveCandle(candle: interfaces.Icandle, market: interfaces.Imarket, exchange: string) {
    let ncandle = [
        Number(candle.timestampMs),
        Number(candle.open),
        Number(candle.high),
        Number(candle.low),
        Number(candle.close),
        Number(candle.volume)
    ]
    let pairId = await db.getPairId(market.symbol, exchange)
    await db.saveDataPoint(pairId, ncandle)
    //console.log('Saved New Candle', market.symbol, candle, ncandle)
    return 'Done!'
}

async function saveTicker(ticker: interfaces.Iticker, exchange: string) {
    let candle = [
        Number(ticker.timestamp),
        Number(ticker.open),
        Number(ticker.high),
        Number(ticker.low),
        Number(ticker.last),
        Number(ticker.quoteVolume)
    ]
    let symbol = ticker.base + '/' + ticker.quote
    let pairId = await db.getPairId(symbol, exchange)
    await db.saveLastDataPoint(pairId, candle)
    //console.log('Saved Latest Price', symbol)
    return 'Done!'
}

async function startSingleMarketCandle(quote: string){
    let exchange = await db.getConfig()
    const ex = new ccxt.binance({
        apiKey: exchange.api_key,
        secret: exchange.api_secret
    })
    const ws = new ccxws.binance({
        apiKey: exchange.api_key,
        secret: exchange.api_secret,
        reconnectIntervalMs: 90000
    }) as any;
    debug ? console.log('starting SingleMarketCandle', quote) :false
    ws.candleInterval = '1m';
    ws.on("candle", (candle: interfaces.Icandle, market: interfaces.Imarket) => {
        saveCandle(candle, market, exchange.exchange)
        //console.log('got candle', candle)
    });

    ws.on("error", (err: any) => console.error(err));

    await sleep(ex.rateLimit)
    ws.on("error", (err: any) => {
        console.error(err)
    });
    exchange = await db.getConfig()
    try{
        for (const market of await ex.fetchMarkets()) {
            if(market.symbol === quote){
                await ws.subscribeCandles(market);
            }
        }
    }catch(err){
        console.log(err)
    }
    
    return 'Done!'
}

async function startSingleMarketPrice(quote: string){
    let exchange = await db.getConfig()
    const ex = new ccxt.binance({
        apiKey: exchange.api_key,
        secret: exchange.api_secret
    })
    const ws = new ccxws.binance({
        apiKey: exchange.api_key,
        secret: exchange.api_secret,
        reconnectIntervalMs: 90000
    }) as any;
    //console.log('starting statLoopWs')
    ws.candleInterval = '1m';
    ws.on("candle", (ticker: interfaces.Iticker) => {
        saveTicker(ticker, exchange.exchange)
        //console.log('got candle', candle)
    });

    ws.on("error", (err: any) => console.error(err));

    await sleep(ex.rateLimit)
    ws.on("error", (err: any) => {
        console.error(err)
    });
    exchange = await db.getConfig()
    try{
        for (const market of await ex.fetchMarkets()) {
            if(market.symbol === quote){
                await ws.subscribeTicker(market);
            }
        }
    }catch(err){
        console.log(err)
    }
    return 'Done!'
}

async function main() {
    //await sleep(60000)
    let exchange = await db.getConfig()
    await updateExchange(exchange as interfaces.Iexchange)
    setInterval(async () => {
        //make one for latest_price only and one for candles only
        let drifted = await db.getDriftedLatestPrice()
        if(drifted.length > 0){
            debug ? console.log('latest_price', drifted) :false
            //restart latest_price
            for(const market of drifted){
                startSingleMarketPrice(market.name)
            }
        }
        drifted = await db.getDriftedCandle()
        if(drifted.length > 0){
            debug ? console.log('candle', drifted) :false
            //restart candle
            for(const market of drifted){
                startSingleMarketCandle(market.name)
            }
        }
        
    }, (2 * 60 * 1000)) // 2min loops
}

main()
 */
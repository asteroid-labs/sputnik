# sputnik

*Sputnik* is the robust backend that bridges exchanges to local TimescaleDB, for blazing-fast queries and no rate-limits.
Major advantages :
	* Will populate historical data of specified markets & exchanges.
	* Will respect whitelist & blacklist of markets.
	* Will fill in holes in the time-series (in case of crash, the next start will downlaod the missing information).
	* Can run audits to assert the integrity of the data compared to the exchange.


## Installation

We'll be using `pnpm` as the package manager, it is *awesome*.

* First, install pnpm globally : `sudo npm install -g pnpm`

** Because I use `spacemacs` i'll follow (these)[https://github.com/syl20bnr/spacemacs/tree/master/layers/%2Blang/typescript] instructions to install the typescript layer

* Then in the project directory, `pnpm install` should install all the dependencies for the project.

* from the `config_example.json`, create your own config file and name it `config.json`

* To compile the project, run `pnpm run tsc` which will translate typescript files to vanilla js


## Preparation

TimescaleDB needs a bit of setup.

* First, install `docker`, as timescale runs on it. See the (docs on the procedure)[https://docs.timescale.com/latest/getting-started/installation/docker/installation-docker].

* Because TimescaleDB is based on PostgresSQL, the same principles apply.

* Start the TimescaleDB docker container using `pnpm run db`

* Create a db user using `psql` (and enter the informations in the config.json)

* Create a database for the sputnik files (I named mine `sputnik` to be original)

* In Config file, you can specify whitelisted market (to use only those) and blacklisted (no to use)

## Run

Simply run `pnpm start` will start sputnik and do it's magic :

* Create all the necessary DB and hypertables

* Subscribe to Websocket for every markets' ticker updates (keeping only the last one updated)

* Periodicaly fetching the final candles (parametrable by config.gap)

* Fetching historical data in 5m buckets (paramtrable by config.minutesMax )


## API Endpoints

Once your sputnik instance is running, you can query it using the provided API endpoints (making sputnik a nice abstraction layer on top of your multiple exchanges modules).

* `/api/exchanges` : Returns a list of the available exchanges and their markets. `[{"exchange":"binance","markets":[{"id":10338331,"name":"ETH/BTC"}]}]`

* `/api/markets` : Returns a list of the available markets and their exchange. `[{"id":10338331,"name":"ETH/BTC","exchange":"binance"]}`

* `/api/markets/:market_id/:timeframe` : Returns a list of candles corresponding to the timeframes for the speficied market. `[{"timed":"1571097600000","opening_price":0.021996,"highest_price":0.02213,"lowest_price":0.02185,"closing_price":0.022119,"volume_base":33581.189}]`

## To Do

* DONE Add admin page to edit config (in process)

* (Mostly DONE) Cleanup code

* Support other exchange (currently only Binance)

* DONE Updated websocket engine to be more stable (waiting on ccxws)

* DONE Add a semi-catch-all on white/black list markets ('ETH' for example)

* DONE create graphs page to compare graphs look with real one (to be reused in vostok

* DONE COMPARE graphs to verify data integrity and bucketing:

* DONE build API endpoints

* Add API endpoints / informations


import * as db from './db/index';
import * as config from './config.json'
import * as child from 'child_process';

async function main() {
    let tables = ['markets', 'markets_data', 'markets_data_latest', 'users', 'config']
    for (const table of tables) {
        await db.checkTables(table)
    }
    await db.createAdminUser(config.web.email, config.web.password)
    await db.saveConfig(
        config.exchanges[0].name,
        config.exchanges[0].key,
        config.exchanges[0].secret,
        JSON.stringify(config.exchanges[0].whitelist),
        JSON.stringify(config.exchanges[0].blacklist),
        config.exchanges[0].history,
        config.exchanges[0].gap,
        config.exchanges[0].worker
    ) // saves the default config to db, to be changed in the web client

    // here we fork different processes
    let forks = ['build/src/historic_data.js', 'build/src/latest_price.js', 'build/src/candles.js', 'build/routes/index.js', 'build/routes/index_https.js']
    for (const f of forks) {
        child.fork(f)
    }
}

main()

import * as db from '../db/index';
import * as interfaces from '../interfaces/index';
import * as ccxt from 'ccxt';
import child from 'child_process';

const debug = false
let sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));
let initialDownload = false

function chunkArray(myArray : [], chunk_size : number){
    var results = [];
    
    while (myArray.length) {
        results.push(myArray.splice(0, chunk_size));
    }
    
    return results;
}

async function shuffleArray(array: []) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array
}

async function loop(ex: any, exchange: interfaces.Iexchange) {
    debug ? console.log('Starting Verifying Data Integrity') :false
    exchange = await db.getConfig() // update to possibly new config
    await sleep(ex.rateLimit)

    let data = await ex.fetchMarkets()
    let arrayAmount = data.length / exchange.worker
    let slicedData = chunkArray(await shuffleArray(data), arrayAmount)// chunkArray(data, arrayAmount)
    //divide await ex.fetchmarkets by workerNb
    for (let [i, d] of slicedData.entries()) {
        try {
            let forked = child.fork('./build/src/sliced_historic_data.js')
            forked.on('message', (msg) => {
                console.log('Message from sliced: "' + msg + '"')
                if(msg === 'Done!'){
                    forked.kill()
                    console.log('killed child process')
                }
              })
            forked.send({exchange: exchange, data : d, i: i })
            // sending trade_id to child process for backtesting
            // save backtesting data to fake_trade table
        } catch (err) {
            console.log(err.stack, 'forking sliced')
        }
    }
}

async function updateExchange(exchange: interfaces.Iexchange) {
    if (exchange.exchange === "binance") {
        const ex = new ccxt.binance({
            apiKey: exchange.api_key,
            secret: exchange.api_secret
        })
        loop(ex, exchange)
        if(!initialDownload){
            setInterval(async () => {
                await loop(ex, exchange)
            }, (10 * 60 * 1000)) //6h loops to verify data
        }
       
    }
}

async function main() {
    let exchange = await db.getConfig()
    await updateExchange(exchange as interfaces.Iexchange)
}

main()

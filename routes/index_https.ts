import passport from 'passport';
import LocalStrategy from 'passport-local';
import path from 'path';
import https from 'https';
import fs from 'fs';
import express from 'express';
import session from 'express-session';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import * as db from '../db/index';
import * as config from '../config.json'
import * as pkg from '../package.json';

// importing sub-routes
import apiRouter from './api';
const compression = require('compression')
const app = express();
app.use(helmet())
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(session({
    secret: config.web.sessionSecret, // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.set('views', './src/views');
app.set('view engine', 'ejs');
app.locals.db = db
app.use('/static', express.static(path.join(__dirname, '../src/public')))
app.use(compression())

// passport.js things

passport.serializeUser(function(user, done) {
    done(null, user); // need to work here
});

passport.deserializeUser(function(user, done) {
    done(null, user); // need to work here
});
passport.use('local', new LocalStrategy.Strategy(
    async function(username, password, done) {
        let user = {} as any
        try {
            user = await db.findUser(username.toLowerCase())
            console.log(user)
        } catch (err) {
            console.log(err.stack)
            return done(err)
        }

        if (!user) {
            console.log('incorrect username.')
            return done(null, false, { message: 'Incorrect username.' });
        }

        if (!db.validPassword(password, user.password)) {
            console.log('incorrect password')
            return done(null, false, { message: 'Incorrect password.' });
        }
        return done(null, user);

    }
));


// express routes

app.get('/', async function(req, res) {
    let marketsCount = await db.getMarketsCount('binance')
    let dataCount = await db.getDataCount()
    let dataCountByMarket = await db.getDataCountByMarkets('binance')
    let version = pkg.version
    let data = { version: version, marketsCount: marketsCount.count, dataCount: dataCount.count, dataCountByMarket: dataCountByMarket }
    res.render('pages/index.ejs', { data });
});


app.get('/login', async function(req, res) {
    res.render('pages/login.ejs')
})


app.post('/login',
    passport.authenticate('local'),
    function(req, res) {
        res.redirect('/admin');
    });


app.get('/admin', async function(req, res) {
    if (req.isAuthenticated()) {
        let config = await db.getConfig()
        res.render('pages/admin.ejs', { config })
    } else {
        res.redirect('/login');
    }
})

app.post('/admin',
    async function(req, res) {
        if (req.isAuthenticated()) {
            let config = await db.getConfig()
            if (req.body.add) {
                // add lists to lists
                req.body.exchange_whitelist = config.whitelist + req.body.exchange_whitelist
                req.body.exchange_blacklist = config.blacklist + req.body.exchange_blacklist
            }
            await db.saveConfig(
                req.body.exchange_name,
                req.body.exchange_api_key,
                req.body.exchange_api_secret,
                JSON.stringify(req.body.exchange_whitelist),
                JSON.stringify(req.body.exchange_blacklist),
                req.body.exchange_history,
                req.body.exchange_gap,
                req.body.exchange_worker
            )

            let new_config = await db.getConfig()
            res.render('pages/admin.ejs', { config: new_config })
        } else {
            // not logged in
            res.redirect('/login');
        }
    }
)

app.get('/markets', async function(req, res) {
    let data = {
        market: 0,
        markets: [],
        timeframe: 0,
        timeframes: [
            '1m',
            '5m',
            '15m',
            '30m',
            '1h',
            '2h',
            '4h',
            '6h',
            '12h',
            '1D',
            '1W',
            '1M'
        ]
    }
    data.market = Number(req.query.market)
    data.timeframe = Number(req.query.timeframe)
    let market_data = []
    if (data.market > 0) {
        market_data = await db.getTimeframesById(data.market, data.timeframe)
    }
    data.markets = await db.getMarkets('binance')
    let n = JSON.stringify(market_data)
    res.render('pages/markets.ejs', { data, n })
})

// api routing
app.use('/api', apiRouter)

if (config.web.https){
https.createServer({
                    key: fs.readFileSync( './cert/server.key' ),
                    cert: fs.readFileSync( './cert/server.pem' )
                }, app).listen(config.web.port_https)
    console.log('sputnik (https) webpage is available on port ' + config.web.port_https + ' !')
}
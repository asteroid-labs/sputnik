import express from 'express';
import * as db from '../db/index';

let router: express.Router = express.Router();
// API endpoints

router.get('/exchanges', async function(req, res) {
    // list exchanges and suported fnc
    let data = await db.getExchanges()
    res.json(data)
})

router.get('/markets', async function(req, res) {
    // list markets and infos
    let data = await db.getMarketList()
    res.json(data)
})

router.get('/markets/:market_id/:timeframe', async function(req, res) {
    // list specific market datapoint by timeframe
    let data = await db.getMarketInTimeframe(Number(req.params.market_id), Number(req.params.timeframe))
    res.json(data)
})

router.get('/markets_by_period/:market_id/:timeframe/:start/:stop', async function(req, res) {
    // list specific market datapoint by timeframe
    let data = await db.getMarketInTimeframePeriod(Number(req.params.market_id), 
                                                    Number(req.params.timeframe), 
                                                    Number(req.params.start), 
                                                    Number(req.params.stop))
    res.json(data)
})

router.get('/markets_by_period_dynamic/:market_id/:start/:stop', async function(req, res) {
    // list specific market datapoint by timeframe
    let data = await db.getMarketInTimeframePeriodDynamic(Number(req.params.market_id), 
                                                    Number(req.params.start), 
                                                    Number(req.params.stop))
    res.json(data)
})

router.get('/markets_last/:market_id/:timeframe/:count', async function(req, res) {
    // list specific market datapoint by timeframe
    let data = await db.getMarketInTimeframeLast(Number(req.params.market_id), Number(req.params.timeframe), Number(req.params.count))
    res.json(data)
})

export = router
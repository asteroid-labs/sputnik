export interface Iexchange {
    exchange: string,
    api_key: string,
    api_secret: string,
    whitelist: string,
    blacklist: string,
    history: number,
    gap: number,
    worker: number
}
export interface Itrade {
    exchange: string,
    base: string,
    quote: string,
    tradeId: string,
    unix: number,
    side: string,
    price: string,
    amount: string,
    buyOrderId: string,
    sellOrderId: string,
}
export interface Iquote {
    //unused
    id: string,
    symbol: string,
    base: string,
    quote: string,
    baseId: string,
    quoteId: string,
    info: {
        symbol: string,
        status: string,
        baseAsset: string,
        baseAssetPrecision: number,
        quoteAsset: string,
        quotePrecision: number,
        orderTypes: [],
        icebergAllowed: boolean,
        ocoAllowed: boolean,
        isSpotTradingAllowed: boolean,
        isMarginTradingAllowed: boolean,
        filters: []
    },
    active: boolean,
    precision: { base: number, quote: number, amount: number, price: number },
    limits: {
        amount: { min: number, max: number },
        price: { min: number, max: number },
        cost: { min: number, max: number | any }
    }

}
export interface Iticker {
    exchange: string,
    base: string,
    quote: string,
    timestamp: number,
    last: number, //string
    open: number,//string
    high: number,//string
    low: number,//string
    volume: number,//string
    quoteVolume: number,//string
    change: number,//string
    changePercent: number,//string
    bid: number,//string
    bidVolume: number,//string
    ask: number,//string
    askVolume: number//string
}

export interface Icandle {
    timestampMs: number,
    open: number,
    high: number,
    low: number,
    close: number,
    volume: number,
}
export interface Imarket {
    id: string,
    symbol: string,
    base: string,
    quote: string,
    baseId: string,
    quoteId: string,
    info: object,
    // {
    //     symbol: 'ATOMBTC',
    //     status: 'TRADING',
    //     baseAsset: 'ATOM',
    //     baseAssetPrecision: 8,
    //     quoteAsset: 'BTC',
    //     quotePrecision: 8,
    //     orderTypes: [
    //         'LIMIT',
    //         'LIMIT_MAKER',
    //         'MARKET',
    //         'STOP_LOSS_LIMIT',
    //         'TAKE_PROFIT_LIMIT'
    //     ],
    //     icebergAllowed: true,
    //     ocoAllowed: true,
    //     isSpotTradingAllowed: true,
    //     isMarginTradingAllowed: true,
    //     filters: [
    //         [Object], [Object],
    //         [Object], [Object],
    //         [Object], [Object],
    //         [Object]
    //     ]
    // },
    active: boolean,
    precision: { base: number, quote: number, amount: number, price: number },
    limits: {
        amount: { min: number, max: number },
        price: { min: number, max: number },
        cost: { min: number, max: number }
    }

}

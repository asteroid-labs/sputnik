#!/bin/bash
echo "**** HAVE YOU FORGOT TO COPY THE CONFIG AND CERTS ?? ****"
docker stop sputnik
docker container rm sputnik
docker run -d --name sputnik-db -v /Users/couteausuisse/GitLab/sputnik/data:/var/lib/postgresql/data --shm-size=256M -p 5433:5432 -e POSTGRES_USER=sputnik -e POSTGRES_DB=sputnik -e POSTGRES_PASSWORD=S3cret --restart=unless-stopped timescale/timescaledb:latest-pg11
docker build -t sputnik .
docker run -d --name sputnik -p 3000:3000 --network="host" --dns=9.9.9.9 --restart=unless-stopped sputnik
docker start sputnik

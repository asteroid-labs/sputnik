
function new_data (data) {
    const margin = { top: 0, right: 0, bottom: 100, left: 50 };
    const width = 1000 - margin.left - margin.right;
    const height = 700 - margin.top - margin.bottom;

    const xScale = d3.scaleBand()
    .domain([0, data.length])
    .range([0, width])
    .round(true)
    .paddingInner(0.3); // space between bars (it's a ratio)

    const yScale = d3.scaleLinear()
    .range([height, 0]);

    const xAxis = d3.axisBottom()
    .scale(xScale)
    .ticks(10);

    const yAxis = d3.axisLeft()
    .scale(yScale)
    .ticks(20);

    const svg = d3.select('div#histogram')
    .append('svg')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', `translate(${margin.left}, ${margin.right})`);

    const tooltip = d3.select('div#histogram').append('div')
    .attr('class', 'tooltip')
    .style('opacity', 0);

    xScale
    .domain(data.map(d => {
      if(+d.count < 1000){
        return 'less than 1000'
      }else{
        return d.name
      }
    }));
    yScale
    .domain([0, d3.max(data, d => +d.count)]);

  svg.append('g')
    .attr('class', 'x axis')
    .attr("x", 9)
    .attr('transform', `translate(0, ${height})`)
    .call(xAxis)
  .selectAll("text")
    .attr("y", 0)
    .attr("x", 9)
    .attr("dy", ".35em")
    .attr("transform", "rotate(90)")
    .style("text-anchor", "start");

    svg.append('g')
    .attr('class', 'y axis')
    .call(yAxis)
    .append('text')
    .attr('transform', 'rotate(-90)')
    .attr('y', 6)
    .attr('dy', '.71em')
    .style('text-anchor', 'end')
    .text('Market');

    svg.selectAll('.bar').data(data)
    .enter()
    .append('rect')
    .attr('class', 'bar')
    .attr('x', d => xScale(d.name))
    .attr('width', xScale.bandwidth())
    .attr('y', d => yScale(d.count))
    .attr('height', d => height - yScale(d.count))
    .on('mouseover', (d) => {
        tooltip.transition().duration(50).style('opacity', 0.9);
        tooltip.html(`Market: ${d.name}, ${d.count}`)
        .style('left', `${d3.event.layerX}px`)
        .style('top', `${(d3.event.layerY - 28)}px`);
    })
    .on('mouseout', () => tooltip.transition().duration(50).style('opacity', 0));

}
import { Pool, Client } from 'pg'
import * as config from '../config.json';
import * as bcrypt from 'bcryptjs';
const debug = false

const pool = new Pool({
    user: config.db.user,
    host: config.db.host,
    database: config.db.db,
    password: config.db.password,
    port: config.db.port
})


export async function checkTables(table_name: string) {
    let status = true
    try {
        let res = await pool.query(`\
                SELECT EXISTS(\
                    SELECT * \
                    FROM information_schema.tables \
                    WHERE table_name = '${table_name}'\
                );`)
        if (!res.rows[0].exists) {
            if (table_name === 'markets') {
                console.log(`no table, creating ${table_name}`)
                try {
                    res = await pool.query(`\
                        create table ${table_name}(\
                        id              SERIAL PRIMARY KEY,\
                        name            TEXT,\
                        exchange        TEXT,\
                        CONSTRAINT unique_markets_con UNIQUE (name, exchange)
                        );\
                        CREATE UNIQUE INDEX unique_markets ON markets(name, exchange);`)
                    debug ? console.log(res.rows[0], 'done create markets') : false // will log all return if debug is on
                } catch (err) {
                    console.log(err.stack, 'error create markets')
                    status = false
                }
            } else if (table_name === 'markets_data') {
                try {
                    res = await pool.query(`\
                        CREATE TABLE ${table_name}(\
                        time            BIGINT NOT NULL,\
                        opening_price   DOUBLE PRECISION,\
                        highest_price   DOUBLE PRECISION,\
                        lowest_price    DOUBLE PRECISION,\
                        closing_price   DOUBLE PRECISION,\
                        volume_base     DOUBLE PRECISION,\
                        pair_id         INTEGER REFERENCES markets (id),\
                        PRIMARY KEY(pair_id, time DESC)\
                        );`)
                    res = await pool.query(`SELECT create_hypertable('markets_data', 'time', chunk_time_interval => 2629800000);`)
                    debug ? console.log(res.rows[0], 'done create markets_data') : false // will log all return if debug is on
                } catch (err) {
                    console.log(err.stack, 'error create markets_data')
                    status = false
                }

            } else if (table_name === 'markets_data_latest') {
                try {
                    res = await pool.query(`\
                        CREATE TABLE ${table_name}(\
                        time            BIGINT NOT NULL,\
                        opening_price   DOUBLE PRECISION,\
                        highest_price   DOUBLE PRECISION,\
                        lowest_price    DOUBLE PRECISION,\
                        closing_price   DOUBLE PRECISION,\
                        volume_base     DOUBLE PRECISION,\
                        pair_id         INTEGER REFERENCES markets (id),\
                        PRIMARY KEY(pair_id), \
                        CONSTRAINT unique_pair_con UNIQUE (pair_id) \
                        );`)
                    debug ? console.log(res.rows[0], 'done create markets_data_latest') : false // will log all return if debug is on
                } catch (err) {
                    console.log(err.stack, 'error create markets_data_latest')
                    status = false
                }

            } else if (table_name === 'users') {
                console.log(`no table, creating ${table_name}`)
                try {
                    res = await pool.query(`\
                        create table ${table_name}(\
                        id              SERIAL PRIMARY KEY,\
                        email           TEXT,\
                        password        TEXT,\
                        CONSTRAINT unique_email_con UNIQUE (email)
                        );`)
                    debug ? console.log(res.rows[0], 'done create users') : false // will log all return if debug is on
                } catch (err) {
                    console.log(err.stack, 'error create users')
                    status = false
                }
            } else if (table_name === 'config') {
                console.log(`no table, creating ${table_name}`)
                try {
                    res = await pool.query(`\
                        create table ${table_name}( \
                        id              INTEGER, \
                        exchange           TEXT, \
                        api_key        TEXT, \
                        api_secret     TEXT, \
                        whitelist      JSON, \
                        blacklist      JSON, \
                        history        INTEGER, \
                        gap            INTEGER, \
                        worker         INTEGER, \
                        CONSTRAINT unique_id UNIQUE (id));`)
                    debug ? console.log(res.rows[0], 'done create config') : false // will log all return if debug is on
                } catch (err) {
                    console.log(err.stack, 'error create config')
                    status = false
                }
        }

        }
    } catch (err) {
        console.log(err.stack)
        status = false
    }
    return status
}

export async function updateQuote(exchange: string, quote: string) {
    // verif that the pair_id corresponding exists (add it if not(UPSERT))
    let result = 0
    try {
        let res = await pool.query(`\
                INSERT INTO markets (exchange, name)\
                VALUES ('${exchange}', '${quote}')
                ON CONFLICT ON CONSTRAINT unique_markets_con DO UPDATE SET name=EXCLUDED.name\
                RETURNING id;`) // upsert
        debug ? console.log(res.rows[0], 'done update quote') : false
        result = res.rows[0].id
    } catch (err) {
        console.log(err.stack, 'error update quote')
    }
    return result
}

export async function saveLastDataPoint(pairId: number, candle: number[]) {
    // verif that the market_data corresponding exists (add it if not(UPSERT))
    // DO UPDATE because websocket will need it (create another function):
    // ON CONFLICT (pair_id, timeframe, time) DO UPDATE\
    // SET highest_price = '${candle[2]}', lowest_price = '${candle[3]}', closing_price = '${candle[4]}', volume_base = '${candle[5]}'
    let res = [] as any
    try {
        res = await pool.query(`\
                INSERT INTO markets_data_latest (time, opening_price, highest_price, lowest_price, closing_price, volume_base, pair_id)\
                VALUES ('${candle[0]}', '${candle[1]}', '${candle[2]}', '${candle[3]}', '${candle[4]}', '${candle[5]}','${pairId}')\
                ON CONFLICT  ON CONSTRAINT unique_pair_con \
                    DO UPDATE SET (time, opening_price, highest_price, lowest_price, closing_price, volume_base, pair_id) = (${candle[0]}, ${candle[1]}, ${candle[2]}, ${candle[3]}, ${candle[4]}, ${candle[5]}, ${pairId}) \
                RETURNING time, pair_id;`)
        if (debug && res.rows.length > 0 && pairId === 1) {
            console.log(res.rows[0], 'done save data new last')
        }
    } catch (err) {
        console.log(err.stack, 'error saveLastDataPoint new last')
    }
    return 'Done!'
}

export async function saveDataPoint(pairId: number, candle: number[]) {
    // verif that the market_data corresponding exists (add it if not(UPSERT))
    // DO UPDATE because websocket will need it (create another function):
    // ON CONFLICT (pair_id, timeframe, time) DO UPDATE\
    // SET highest_price = '${candle[2]}', lowest_price = '${candle[3]}', closing_price = '${candle[4]}', volume_base = '${candle[5]}'
    let res = [] as any

        if(Number(candle[1]) === 0 && Number(candle[2]) === 0 && Number(candle[3]) === 0 && Number(candle[4]) === 0 && Number(candle[5]) === 0){
            try {
                res = await pool.query(`\
                    INSERT INTO markets_data (time, pair_id)\
                    VALUES (${candle[0]}, ${pairId})\
                    ON CONFLICT DO NOTHING \
                    RETURNING time, pair_id;`) // upsert
                if (debug && res.rows.length > 0) {
                    console.log(candle, res.rows[0], 'done save data')
                }
            } catch (err) {
                console.log(err.stack, 'error saveDataPoint null', candle)
            }
        }else{
            try {
                res = await pool.query(`\
                    INSERT INTO markets_data (time, opening_price, highest_price, lowest_price, closing_price, volume_base, pair_id)\
                    VALUES (${candle[0]}, ${candle[1]}, ${candle[2]}, ${candle[3]}, ${candle[4]}, ${candle[5]}, ${pairId})\
                    ON CONFLICT (pair_id, time) \
                        DO UPDATE SET (time, opening_price, highest_price, lowest_price, closing_price, volume_base, pair_id) = (${candle[0]}, ${candle[1]}, ${candle[2]}, ${candle[3]}, ${candle[4]}, ${candle[5]}, ${pairId}) \
                    RETURNING time, pair_id;`) // upsert
                if (debug && res.rows.length > 0) {
                    console.log(candle, res.rows[0], 'done save data')
                }
            } catch (err) {
                console.log(err.stack, 'error saveDataPoint', candle)
            }
        }
    return 'Done!'
}


export async function getTimeGaps(pairId: number) {
    let res = {} as any
    try {
        let first = await pool.query(`select time from markets_data where pair_id = ${pairId} order by time asc LIMIT 1;`)
        let last = await pool.query(`select time from markets_data where pair_id = ${pairId} order by time desc LIMIT 1;`)
        if(first.rows.length > 0 && last.rows.length > 0){
        res = await pool.query(`SELECT time_bucket_gapfill(60000, time, start => '${first.rows[0].time}', finish => '${last.rows[0].time}') AS timed, \
                    COUNT(*) \
                FROM markets_data \
                WHERE pair_id = ${pairId} AND time BETWEEN ${first.rows[0].time} AND ${last.rows[0].time} \
                GROUP BY timed ORDER BY timed DESC;`)
        }
    } catch (err) {
        console.log(err.stack)
    }
    if(res?.rows?.length > 0){
        return res.rows.filter((row:{timed: number, count: number}) => row.count != 1)
    } else {
        return []
    }
}

/* export async function getEmptyTimeGaps(pairId: number) {
    let res = {} as any
    try {
        let first = await pool.query(`select time from markets_data where pair_id = ${pairId} order by time asc LIMIT 1;`)
        let last = await pool.query(`select time from markets_data where pair_id = ${pairId} order by time desc LIMIT 1;`)
        res = await pool.query(` \
            SELECT * FROM (\
                SELECT time_bucket_gapfill(60000, time, start => '${first.rows[0].time}', finish => '${last.rows[0].time}') AS timed, \
                    COUNT(*) as count \
                FROM markets_data \
                WHERE pair_id = ${pairId} AND time BETWEEN ${first.rows[0].time} AND ${last.rows[0].time} \
                GROUP BY timed ) as grouped \
            WHERE grouped.count IS NULL;`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows
} */

export async function get(pairId: number, time: number) {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * FROM markets_data WHERE pair_id = ${pairId} AND time = ${time};`)
    } catch (err) {
        console.log(err.stack)
    }
    //console.log(res.rows)
    return res.rows[0]
}

export async function getTimeframesById(pairId: number, timeframe: number) {
    let res = {} as any
    let timeframes = [
        60000, // 1m
        300000, // 5m
        900000, // 15m
        1800000, // 30m
        3600000, // 1h
        7200000, // 2h
        14400000, // 4h
        21600000, // 6h
        43200000, // 12h
        86400000, // 1D
        604800000, // 1W
        2628002880, // ~1M
    ]
    try {
        res = await pool.query(`SELECT time_bucket(${timeframes[timeframe]}, time) AS timed, \
                first(opening_price, time) AS "opening_price", \
                max(highest_price) AS "highest_price", \
                min(lowest_price) AS "lowest_price", \
                last(closing_price, time) AS "closing_price", \
                sum(volume_base) AS "volume_base" \
            FROM markets_data \
            WHERE pair_id = ${pairId} \
            AND opening_price IS NOT NULL \
            GROUP BY timed \
            ORDER BY timed DESC;`)
    } catch (err) {
        console.log(err.stack, 'error getTimeframesById')
    }

    return res.rows
}

export async function getPairId(quote: string, exchange: string) {
    await updateQuote(exchange, quote)
    let res = {} as any
    try {
        res = await pool.query(`\
 SELECT id, name, exchange FROM markets WHERE name = '${quote}' AND exchange = '${exchange}';`)
        // debug ? console.log(res.rows[0], 'done getPairId') : false
    } catch (err) {
        console.log(err.stack, 'error getPairId')
    }
    return res.rows[0].id
}

export async function getMarketsCount(exchange: string) {
    let res = {} as any
    try {
        res = await pool.query(`SELECT COUNT(*) FROM markets WHERE exchange = '${exchange}';`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows[0]
}
export async function getDataCountByMarkets(exchange: string) {
    let res = {} as any
    try {
        res = await pool.query(` SELECT name, counted.count FROM markets \
            JOIN (select pair_id, count(*) from markets_data \
                GROUP BY pair_id ORDER BY count(*) DESC \
                ) as counted ON counted.pair_id = markets.id 
            WHERE exchange = '${exchange}'  \
            GROUP BY name, counted.count \
            ORDER BY counted.count DESC;`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows
}

export async function getMarkets(exchange: string) {
    let res = {} as any
    try {
        res = await pool.query(`SELECT id, name FROM markets WHERE exchange = '${exchange}' ORDER BY name;`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows
}
export async function getDataCount() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT COUNT(*) FROM markets_data;`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows[0]
}
export async function getConfig() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * FROM config WHERE id = 1;`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows[0]
}
export async function saveConfig(name: string, api_key: string, api_secret: string, whitelist: string, blacklist: string, history: number, gap: number, worker: number) {
    let res = {} as any
    try {
        res = await pool.query(`\
            INSERT INTO config (id, exchange, api_key, api_secret, whitelist, blacklist, history, gap, worker)\
            VALUES (1, '${name}', '${api_key}', '${api_secret}', '${whitelist}', '${blacklist}', ${history}, ${gap}, ${worker})\
            ON CONFLICT ON CONSTRAINT unique_id DO UPDATE SET exchange=EXCLUDED.exchange, api_key=EXCLUDED.api_key, \
                api_secret=EXCLUDED.api_secret, whitelist=EXCLUDED.whitelist, blacklist=EXCLUDED.blacklist, \ 
                history=EXCLUDED.history, gap=EXCLUDED.gap, worker=EXCLUDED.worker;`) // upsert
        if (debug && res.rows.length > 0) {
            console.log(res.rows[0], 'done save config')
        }
    } catch (err) {
        console.log(err.stack, 'error config')
    }

}

export async function createAdminUser(email: string, password: string) {
    let res = {} as any
    bcrypt.hash(password, 8, async function(err, hash) {
        try {
            res = await pool.query(`\
                INSERT INTO users (email, password)\
                VALUES ('${email}', '${hash}')\
                ON CONFLICT DO NOTHING;`) // upsert
            if (debug && res.rows.length > 0) {
                console.log(res.rows[0], 'done save user')
            }
        } catch (err) {
            console.log(err.stack, 'error user')
        }
    });

}
export async function validPassword(password: string, hash: string) {
    bcrypt.compare(password, hash, function(err, res) {
        // res === true
        return res
    });

}

export async function findUser(username: string) {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * from users where email = '${username}'`)
    } catch (err) {
        console.log(err.stack)
        return err
    }
    return res.rows[0]
}


// API

export async function getExchanges() {
    let exchanges = [] as any
    try {
        let data = {} as any
        data = await pool.query(`SELECT distinct exchange from markets`)
        for (const ex of data.rows) {
            exchanges.push(ex)
        }
    } catch (err) {
        console.log(err.stack)
    }
    for (const [i, ex] of exchanges.entries()) {
        exchanges[i].markets = []
        try {
            let data = {} as any
            data = await pool.query(`SELECT id, name FROM markets WHERE exchange = '${ex.exchange}' ORDER BY id`)
            for (const mark of data.rows) {
                exchanges[i].markets.push(mark)
            }
        } catch (err) {
            console.log(err.stsck)
        }
    }
    return exchanges
}


export async function getMarketList() {
    let res = {} as any
    try {
        res = await pool.query(`SELECT * FROM markets ORDER BY id`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows
}

export async function eraseMarket(id: number) {
    let res = {} as any
    try {
        res = await pool.query(`DELETE FROM markets_data WHERE pair_id = '${id}'; DELETE FROM markets_data_latest WHERE pair_id = '${id}'; DELETE FROM markets WHERE id = '${id}';`)
    } catch (err) {
        console.log(err.stack)
    }
    return res.rows
}

/* export async function getMarket(market_id: number) {
    let res = {} as any
    let data = []
    try {
        res = await pool.query(`SELECT * FROM (SELECT * FROM markets_data WHERE pair_id = '${market_id}' AND opening_price IS NOT NULL \
            UNION \
            SELECT * FROM markets_data_latest WHERE pair_id = '${market_id}') as market \
            ORDER BY market.time DESC`)
        for (const point of res.rows) {
            data.push(point)
        }
    } catch (err) {
        console.log(err.stack)
    }
    return data
}
 */

export async function getMarketInTimeframe(market_id: number, timeframe: number) {
    let res = {} as any
    let data = []

    try {
        res = await pool.query(` \
            SELECT time_bucket(${timeframe}, market.time) AS timed, \
                first(market.opening_price, market.time) AS "opening_price", \
                max(market.highest_price) AS "highest_price", \
                min(market.lowest_price) AS "lowest_price", \
                last(market.closing_price, time) AS "closing_price", \
                sum(market.volume_base) AS "volume_base" \
            FROM ( \
                    SELECT * FROM markets_data  \
                        WHERE markets_data.pair_id = ${market_id} \
                         AND markets_data.opening_price IS NOT NULL \
                        ORDER BY markets_data.time DESC
                    UNION  \
                    SELECT * FROM markets_data_latest WHERE markets_data_latest.pair_id = ${market_id}) as market \
            GROUP BY timed \
            ORDER BY timed DESC;`)
        for (const point of res.rows) {
            data.push(point)
        }
    } catch (err) {
        console.log(err.stack, 'error getMarketInTimeframe')
    }
    return data
}


export async function getMarketInTimeframePeriod(market_id: number, timeframe: number, start: number, stop: number) {
    let res = {} as any
    let data = []

    try {
        res = await pool.query(` \
            SELECT time_bucket(${timeframe}, market.time) AS timed, \
                first(market.opening_price, market.time) AS "opening_price", \
                max(market.highest_price) AS "highest_price", \
                min(market.lowest_price) AS "lowest_price", \
                last(market.closing_price, market.time) AS "closing_price", \
                sum(market.volume_base) AS "volume_base" \
            FROM (  \
                (SELECT * FROM markets_data  \
                    WHERE markets_data.pair_id = ${market_id} \
                        AND markets_data.opening_price IS NOT NULL \
                        AND markets_data.time BETWEEN ${start} AND ${stop} \
                    ORDER BY markets_data.time DESC)
                UNION \
                (SELECT * FROM markets_data_latest WHERE markets_data_latest.pair_id = ${market_id})) as market \
            WHERE market.time BETWEEN ${start} AND ${stop} \
            GROUP BY timed \
            ORDER BY timed DESC;`)
        for (const point of res.rows) {
            data.push(point)
        }
    } catch (err) {
        console.log(err.stack, 'error getMarketInTimeframePeriod', market_id, timeframe, start, stop)
    }
    return data
}

export async function getMarketInTimeframePeriodDynamic(market_id: number, start: number, stop: number) {
    let res = {} as any
    let data = []
    // find an appropriate timeframe for the time gap
    
    let timeframe = 60000
    let time_gap = (stop-start) / timeframe // number of 1m candles

    while(time_gap > 1000){
        // 2 weeks in 1m
        // in 10m
        // in 1h30
        // in 15h

        timeframe = timeframe * 10
        time_gap = (stop-start) / timeframe
    }

    try {
        res = await pool.query(` \
            SELECT time_bucket(${timeframe}, market.time) AS timed, \
                first(market.opening_price, market.time) AS "opening_price", \
                max(market.highest_price) AS "highest_price", \
                min(market.lowest_price) AS "lowest_price", \
                last(market.closing_price, market.time) AS "closing_price", \
                sum(market.volume_base) AS "volume_base" \
            FROM (  \
                (SELECT * FROM markets_data  \
                    WHERE markets_data.pair_id = ${market_id} \
                        AND markets_data.opening_price IS NOT NULL \
                        AND markets_data.time BETWEEN ${start} AND ${stop} \
                    ORDER BY markets_data.time DESC)
                UNION \
                (SELECT * FROM markets_data_latest WHERE markets_data_latest.pair_id = ${market_id})) as market \
            WHERE market.time BETWEEN ${start} AND ${stop} \
            GROUP BY timed \
            ORDER BY timed DESC;`)
        for (const point of res.rows) {
            data.push(point)
        }
    } catch (err) {
        console.log(err.stack, 'error getMarketInTimeframePeriod', market_id, timeframe, start, stop)
    }
    return data
}

export async function getMarketInTimeframeLast(market_id: number, timeframe: number, count: number) {
    let res = {} as any
    let data = []
    try {
        res = await pool.query(`\
        SELECT time_bucket(${timeframe}, market.time) AS timed, \
            first(market.opening_price, market.time) AS "opening_price", \
            max(market.highest_price) AS "highest_price", \
            min(market.lowest_price) AS "lowest_price", \
            last(market.closing_price, market.time) AS "closing_price", \
            sum(market.volume_base) AS "volume_base" \
        FROM ( \
                (SELECT * FROM markets_data  \
                    WHERE markets_data.pair_id = ${market_id} \
                    AND markets_data.opening_price IS NOT NULL \
                    ORDER BY markets_data.time DESC \
                    LIMIT ${(count * (timeframe/60000)) > 1 ? ((count * (timeframe/60000)) - 1) : 1 }) \
                UNION \
             (SELECT * FROM markets_data_latest WHERE markets_data_latest.pair_id = ${market_id})) as market \
        GROUP BY timed \
        ORDER BY timed DESC;`)
        for (const point of res.rows) {
            data.push(point)
        }
    } catch (err) {
        console.log(err.stack, 'error getMarketInTimeframeLast')
    }
    return data
}


/* export async function getDriftedLatestPrice(){
    let drifted = []
    // get all markets id
    let markets = await getMarketList()
    // get date
    let d = new Date().valueOf()
    // if market.time < date-60000 (we tolerate 1m drifts)
    for(const market of markets){
        let data = {} as any
        try{
            data = await pool.query(`SELECT * FROM markets_data_latest WHERE markets_data_latest.pair_id = ${market.id}`)
            data = data.rows[0]
        }catch(err){
            console.log(err, 'error get latest_price')
        }
        //console.log(data)
        if(typeof data.time !== 'undefined'){
            if(Number(data.time) < (d - 120000)){
                drifted.push(market)
            }
        }
    }
    return drifted
}

export async function getDriftedCandle(){
    let drifted = []
    // get all markets id
    let markets = await getMarketList()
    // get date
    let d = new Date().valueOf()
    // if market.time < date-60000 (we tolerate 1m drifts)
    for(const market of markets){
        let data = await getMarketInTimeframeLast(market.id, 60000, 3)
        // skip latest_price

        if(data.length > 1){
            if(Number(data[data.length - 1].timed) < (d - 120000)){
                drifted.push(market)
            }
        }
    }
    return drifted
} */
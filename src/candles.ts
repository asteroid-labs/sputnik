//import * as config from './config.json';
import * as db from '../db/index';
import * as interfaces from '../interfaces/index';
import * as ccxt from 'ccxt';
import * as ccxws from 'ccxws';

let sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

async function saveCandle(candle: interfaces.Icandle, market: interfaces.Imarket, exchange: string) {
    let ncandle = [
        Number(candle.timestampMs),
        Number(candle.open),
        Number(candle.high),
        Number(candle.low),
        Number(candle.close),
        Number(candle.volume)
    ]
    let pairId = await db.getPairId(market.symbol, exchange)
    await db.saveDataPoint(pairId, ncandle)
    //console.log('Saved New Candle', market.symbol, candle, ncandle)
    return 'Done!'
}

async function loopWS(ex: any, ws: any, exchange: interfaces.Iexchange) {
    
    await sleep(ex.rateLimit)
    ws.on("error", (err: any) => {
        console.error(err)
    });
    exchange = await db.getConfig()
    for (const quote of await ex.fetchMarkets()) {
        if ((exchange.whitelist.includes(quote.symbol) ||
            (exchange.whitelist.includes(quote.quote) || exchange.whitelist.includes(quote.base)) ||
            exchange.whitelist.length === 0)
            && !exchange.blacklist.includes(quote.symbol)
            && !(exchange.blacklist.includes(quote.quote) || exchange.blacklist.includes(quote.base))
            && quote.active) {
            await ws.subscribeCandles(quote);
            //console.log('Subscribing for Candles', quote.symbol)
        }
    }
    return 'done!'
}

async function startLoopWs(exchange: interfaces.Iexchange, ex: any) {
    const ws = new ccxws.binance({
        apiKey: exchange.api_key,
        secret: exchange.api_secret,
        reconnectIntervalMs: 90000
    }) as any;
    //console.log('starting statLoopWs')
    ws.candleInterval = '1m';
    ws.on("candle", (candle: interfaces.Icandle, market: interfaces.Imarket) => {
        saveCandle(candle, market, exchange.exchange)
        //console.log('got candle', candle)
    });

    ws.on("error", (err: any) => console.error(err));

    await loopWS(ex, ws, exchange)
    return 'Done!'
    //ws.close()
}

async function updateExchange(exchange: interfaces.Iexchange) {
    if (exchange.exchange === "binance") {
        const ex = new ccxt.binance({
            apiKey: exchange.api_key,
            secret: exchange.api_secret
        })
        await startLoopWs(exchange, ex)
      /*   setInterval(async () => {
            await startLoopWs(exchange, ex)
        }, (10 * 60 * 1000)) // 25min loops */

    }
}

async function main() {
    let exchange = await db.getConfig()
    await updateExchange(exchange as interfaces.Iexchange)
}

main()

//import * as config from './config.json';
import * as db from '../db/index';
import * as interfaces from '../interfaces/index';
import * as ccxt from 'ccxt';
import * as ccxws from 'ccxws';

const debug = false

let sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

async function saveTicker(ticker: interfaces.Iticker, exchange: string) {
    let candle = [
        Number(ticker.timestamp),
        Number(ticker.open),
        Number(ticker.high),
        Number(ticker.low),
        Number(ticker.last),
        Number(ticker.quoteVolume)
    ]
    let symbol = ticker.base + '/' + ticker.quote
    let pairId = await db.getPairId(symbol, exchange)
    await db.saveLastDataPoint(pairId, candle)
    //console.log('Saved Latest Price', symbol)
    return 'Done!'
}

async function loopWS(ex: any, ws: any, exchange: interfaces.Iexchange) {
    //console.log('Starting WebSockets for latest price')
    await sleep(ex.rateLimit)
    exchange = await db.getConfig()
    for (const quote of await ex.fetchMarkets()) {
        if ((exchange.whitelist.includes(quote.symbol) ||
            (exchange.whitelist.includes(quote.quote) || exchange.whitelist.includes(quote.base)) ||
            exchange.whitelist.length === 0)
            && !exchange.blacklist.includes(quote.symbol)
            && !(exchange.blacklist.includes(quote.quote) || exchange.blacklist.includes(quote.base))
            && quote.active) {
            // console.log('subscribre to latest_price ', quote.symbol)
            await ws.subscribeTicker(quote);
            debug ? console.log('Subscribing for Ticker', quote.symbol) : false
        }
    }
    return 'done!'
}

async function startLoopWs(exchange: interfaces.Iexchange, ex: any) {
    const ws = new ccxws.binance({
        apiKey: exchange.api_key,
        secret: exchange.api_secret,
        reconnectIntervalMs: 90000
    }) as any;
    //console.log('starting statLoopWs')
    ws.on("ticker", (ticker: interfaces.Iticker) => {
        saveTicker(ticker, exchange.exchange)
        //console.log('got price', ticker.timestamp)
    });
    ws.on("error", (err: any) => console.error(err));

    await loopWS(ex, ws, exchange)
    return 'Done!'
    // ws.close()
}

async function updateExchange(exchange: interfaces.Iexchange) {
    if (exchange.exchange === "binance") {
        const ex = new ccxt.binance({
            apiKey: exchange.api_key,
            secret: exchange.api_secret
        })
        await startLoopWs(exchange, ex)

       /*  setInterval(async () => {
            await startLoopWs(exchange, ex)
        }, (10 * 60 * 1000)) //30m loops to reconnect  */
    }
}

async function main() {
    let exchange = await db.getConfig()
    await updateExchange(exchange as interfaces.Iexchange)
}

main()

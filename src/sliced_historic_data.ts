//import * as config from './config.json';
import * as db from '../db/index';
import * as interfaces from '../interfaces/index';
import * as ccxt from 'ccxt';

const debug = false
let sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

async function fetchMarketData( exchange: interfaces.Iexchange, pairId: number, symbol: string, missing: number, debug: boolean ) {
    const CCXT = ccxt as any
    const ex = new CCXT[exchange.exchange]({
        apiKey: exchange.api_key,
        secret: exchange.api_secret,
        'timeout': 30000,
        'enableRateLimit': true,
    }) as ccxt.Exchange

    let data = [] as number[][]
    //while loop with pushback
    let result = false
    let times = 1
    while(!result){
        await sleep(ex.rateLimit * times)
        try{
            data = await ex.fetchOHLCV(symbol, '1m', missing)
            result = true
        }catch(err){
            console.log(err, 'error fetchOHCLV', symbol, '1m', missing)
            times = times * 2
            result = false
        }
    }
    if(data.length > 1){
       if(debug){
        debug ? console.log('Fetching data for', symbol, '(', pairId, ')', new Date(Number(missing)), 'got', new Date(Number(data[0][0])), 'to', new Date(Number(data[data.length-1][0]))) : false
        }
        for await (const candle of data) {
            await db.saveDataPoint(pairId, candle) // saving not-last datapoint
/*             let db_data = await db.get(pairId, candle[0])
            if(typeof db_data === 'undefined'){
                console.log('candle not saved or non existing', candle)
            } */
        } 
    }else{
        console.log('data empty', data)
    }
    
    return {nb:data.length, last:data[data.length - 1][0], first: data[0][0]}
}


async function verifyContinuity(exchange: interfaces.Iexchange, pairId: number, symbol: string, debug: boolean) {
    let data = await db.getTimeGaps(pairId)
    //console.log(data)
    if(data.length > 1){
        let last_changed = 0
        let skip = false
        for(const point of data){
            if (point.count === null && !skip) {
                // this time value have no registedred candles in the db

                // make sure the time value haven't been updated by a previous candle
                let db_data = await db.get(pairId, point.timed)

                if(typeof db_data === 'undefined'){
                    // time value is still empty
                    //console.log('time value still empty', pairId, point.timed)
                    
                    let ndata = await fetchMarketData(exchange, pairId, symbol, Number(point.timed-(499*60000)), false)
                    if(debug){
                        //console.log('Fetched','(+',ndata.nb,')','data points for', symbol, '(', pairId, ') between', new Date(ndata.first),ndata.first, 'and', new Date(Number(ndata.last)),ndata.last)
                    }  
                    if(Number(ndata.last) != Number(point.timed)){
                        // returned value is different the value asked
                        // console.log('returned value is different than value asked', Number(ndata.last), Number(point.timed))
                        let missing_nb = Number(((Number(ndata.last) - Number(point.timed)) / 60000).toFixed())
                        for (let index = 0; index < missing_nb; index++) {
                            let candle = [Number(Number(point.timed)+Number(index * 60000)), 0, 0, 0, 0, 0]
                            await db.saveDataPoint(pairId, candle)
                        }
                    }
                    if(last_changed !== Number(ndata.first)){
                        // period exists
                        last_changed = Number(ndata.first)
                    }else{
                        // reached binance limit
                        if(debug){
                            console.log('Reached Market end for', symbol, '(', pairId, ') at', new Date(Number(ndata.last)),ndata.last)
                        } 
                        skip = true
                    }
                }else{
                    //console.log('candle already saved', symbol, new Date(Number(point.timed)))
                }
            } else{
                // this time value is already saved
                //console.log('time value already saved')
            }
        }
    }else{
        console.log(symbol, '(', pairId, ') gaps are non existant, wonderfull!')
    }
    return 'done!'
}


async function loop(exchange: interfaces.Iexchange, data: any, i: number) {
    debug ? console.log('worker #', i, ': Starting Verifying Data Integrity', data.length) : false
    for (let [j,quote] of data.entries()) {
        if (
            (exchange.whitelist.includes(quote.symbol) || (exchange.whitelist.includes(quote.quote) || exchange.whitelist.includes(quote.base)) || exchange.whitelist.length === 0)
            && !(exchange.blacklist.includes(quote.symbol) || (exchange.blacklist.includes(quote.quote) || exchange.blacklist.includes(quote.base)))
            && quote.info.status === 'TRADING'
        ) {
            
            // get pairId or create one and get this one's
            let pairId = await db.updateQuote(exchange.exchange, quote.symbol)
            // get historic data from config time ago (or fill in gaps)
            await fetchMarketData(exchange, pairId, quote.symbol, Number(Date.now() - 180000), debug) // get 3 last minutes
            await fetchMarketData(exchange, pairId, quote.symbol, Number(Date.now() - (exchange.history * 60 * 1000)), debug) // get history point
            // verify data coontinuity
            await verifyContinuity(exchange, pairId, quote.symbol, debug)
        }else{
         /*    console.log(quote.symbol, (exchange.whitelist.includes(quote.symbol) || (exchange.whitelist.includes(quote.quote) || exchange.whitelist.includes(quote.base)) || exchange.whitelist.length === 0))
            console.log(quote.symbol, !(exchange.blacklist.includes(quote.symbol) || (exchange.blacklist.includes(quote.quote) || exchange.blacklist.includes(quote.base))), quote.active)
            console.log(quote.symbol, (exchange.whitelist.includes(quote.symbol) || (exchange.whitelist.includes(quote.quote) || exchange.whitelist.includes(quote.base)) || exchange.whitelist.length === 0) && !(exchange.blacklist.includes(quote.symbol) || (exchange.blacklist.includes(quote.quote) || exchange.blacklist.includes(quote.base))) && quote.active )
            console.log(quote) */
            // remove market
            let pairId = await db.updateQuote(exchange.exchange, quote.symbol)
            await db.eraseMarket(pairId)
        }

    }
    return 'done!'
}


process.on('message', async function(msg) {
    //console.log('Message from parent:', msg);
    if(msg.data){
        await loop(msg.exchange, msg.data, msg.i)
    }
    debug ? console.log('worker #', msg.i, ': historic data worker done, exiting') : false
    process.exit()
  });
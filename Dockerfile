FROM node:13

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .
COPY config.json ./build
COPY cert/* ./build/cert/

#EXPOSE 3000

CMD ["npm", "start"]
